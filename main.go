package main

import (
    "fmt"
    "strings"
)

func main() {
    m := []string{"1", "2", "3", "4"}
    fmt.Println(strings.Join(m, ","))

}
